﻿@script ExecuteInEditMode()

var menu = true; 
var options = false;
var sound = false;
var video = false;

var sfxVol : int = 6;
var musicVol : int = 6;

var fieldOfView : int = 80;

var style : GUIStyle;

function Start(){
	style.normal.textColor = Color.black;
}

function OnGUI () {

	if(menu){
		
			if (GUI.Button(Rect(Screen.width/2 - 50, Screen.height/2, 100, 30), "Start Game")){
			
				Application.LoadLevel(1);
			}
		
			if (GUI.Button(Rect(Screen.width/2 - 50, Screen.height/2 + 30, 100, 30), "Options")){
		
				menu = false;
				options = true;
			}
		
			if (GUI.Button(Rect(Screen.width/2 - 50, Screen.height/2 + 90, 100, 30), "Quit")){
		
				Application.Quit();
		}
	}
	
	if(options){
	
		if (GUI.Button(Rect(Screen.width/2 - 50, Screen.height/2, 100, 30), "Audio Settings")){
		
			options = false;
			sound = true;
		}
		
		if (GUI.Button(Rect(Screen.width/2 - 50, Screen.height/2 + 30, 100, 30), "Video Settings")){
			
			options = false;
			video = true;
		}
		
		if (GUI.Button(Rect(Screen.width/2 - 50, Screen.height/2 + 90, 100, 30), "Back")){	
	
			options = false;
			menu = true;
		}
	}
		
	if(sound){
		
		sfxVol = GUI.HorizontalSlider (Rect (Screen.width/2 - 50, Screen.height/2, 100, 30), sfxVol, 0.0, 10.0);
		GUI.Label (Rect(Screen.width/2 - 50 + 110, Screen.height/2 - 5, 100, 30), ("SFX: " + sfxVol), style);
			
		musicVol = GUI.HorizontalSlider (Rect (Screen.width/2 - 50, Screen.height/2 + 25, 100, 30), musicVol, 0.0, 10.0);
		GUI.Label (Rect (Screen.width/2 - 50 + 110, Screen.height/2 + 25, 100, 30), ("Music: " + musicVol), style);
			
		if (GUI.Button(Rect(Screen.width/2 - 50, Screen.height/2 + 90, 100, 30), "Back")){
			
			sound = false;
			options = true;
			
		
		}
		
	}
		
	if(video){
		
		var qualities = QualitySettings.names;
			
		GUILayout.BeginVertical ();
			
		for (var i = 0; i < qualities.Length; i++){
			
			if (GUI.Button(Rect(Screen.width/2 - 50, Screen.height/2 - 120 + i * 30, 100, 30), qualities[i]))
			{
				QualitySettings.SetQualityLevel (i, true);
			}
		}	
	
		
		GUILayout.EndVertical ();
		
		fieldOfView = GUI.HorizontalSlider (Rect (Screen.width/2 - 50, Screen.height/2 - 150,100,20), fieldOfView, 30, 120);
		GUI.Label(Rect(Screen.width/2 - 50, Screen.height/2 + 90, 100, 30), "Back");{
		
			video = true;
			options = false;
			menu = false;
			
		}
		if (GUI.Button(Rect(Screen.width/2 - 50, Screen.height/2 + 190, 100, 30), "Back")){	
	
			video = false;
			options = true;
		}
	}		
}	