﻿#pragma strict

var coinEffect : Transform;
static var score = 0;

var offsetY : float = 40;
var sizeX : float = 100;
var sizeY : float = 40;

function OnTriggerEnter (other : Collider)
{
	
	Destroy(gameObject);
	score+=1;
	OnGUI();
	
}

function OnGUI () {
	GUI.Box (new Rect (Screen.width/2-sizeX/2, offsetY, sizeX, sizeY), "Score: " + CoinPickup.score);
}